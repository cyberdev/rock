# R.O.C.K.

Response Operations Collections Kit - A _kitchen_ for a Linux distro that
enables quick configuration of a streamlined, passive network sensor, tuned for
incident response, though could be used for more sustained operations. Note that
 this is focused on EL7, but will likely be applicable for Fedora 20+ as well.

**UNDER DEVELOPMENT**

## The Parts
- Source repo: https://bitbucket.org/cyberdev/rock

!!!note
  Not all parts are available in this repo yet. Namely a couple of the other
  cookbooks aren't on bitbucket or github. That will eventually change. Contact
  me directly if you'd like the missing pieces.

## The Scheme
The idea is to "Chef-itize" a deployment bash script that I wrote to build the
base platform. Once that part is reliable and you, my friends, can just get it
going in Vagrant or just as a VM, then I'll continue working on the idea list
below. Yes. Puppet is dead, going with Chef from here on out.

### Remote Deployment

#### Dependencies

In order to use this repository to deploy machines, you will need the following
dependencies installed and configured on your deployment machine:

* ChefDK <<https://downloads.chef.io/chef-dk/mac/>>
* knife zero <<http://knife-zero.github.io/10_install/>>

### bootstrap

~~~~~~~~~
$ berks vendor cookbooks/
$ knife zero bootstrap <ip or fqdn>  -N <name of node> -x <ssh user> --sudo -r "role[rock-collector]"
~~~~~~~~~

### Local Development

#### Dependencies

In order to use this repository locally, you will need the following
dependencies installed and configured:

* Vagrant <<https://www.vagrantup.com/downloads.html>>
* VirtualBox <<https://www.virtualbox.org/wiki/Downloads>>
* ChefDK <<https://downloads.chef.io/chef-dk/mac/>>

**IMPORTANT NOTE**

This configuration currently uses the Opscode Centos-7.1 Vagrant box. To install
 locally, run the following after installing Vagrant & VirtualBox
```
vagrant box add http://opscode-vm-bento.s3.amazonaws.com/vagrant/virtualbox/opscode_centos-7.1_chef-provisionerless.box --name opscode/centos-7.1
```
#### Vagrantfile

The Vagrantfile provided with this repository is currently configured to build
(1) nodes, in a local private network. The nodes are as follows
* rock <192.168.168.11>

These nodes are defined as Chef "roles" and can be viewed under the './roles'
directory. These roles inherit the customized cookbooks for the project, which
can be found in the './site-cookbooks' directory.

#### Cookbooks

The "cookbooks" directory contains Chef community (OSS), and
custom cookbooks, which have been vendored using the Berkshelf tool.

#### Site Cookbooks

The "site-cookbooks" directory contains the customized Chef wrapper cookbooks
for the project. These cookbooks inherit their basic configurations from the
"cookbooks" directory. All customizations for this project will be done within
these "site-cookbooks"

#### Running Locally in Vagrant

Once all of the required dependencies have been met, building the nodes locally
is rather simple. Using the Vagrant tool, you can run the following commands

```
$ vagrant status
Current machine states:

rock                      not created (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```
* vagrant status - shows the available machines, and their existing states

```
$ vagrant up
```
* vagrant up - when run without specifying a machine-type, will build and launch
 ALL machines available to Vagrant

```
$ vagrant up rock
```
* vagrant up (machine-name) - will spin up individual machines, on demand

```
$ vagrant destroy
```
* vagrant destroy - when run without specifying a machine-type, will destroy
ALL running machines

#### Modifying the Chef Cookbooks (Example)

The individual 'site-cookbooks' may be modified if desired to add additional
functionality. Below is an example of how this can be performed.


####### Old stuff, for reference


## TODOs (should be moved to issue tracker/milestone planner)

- Build-Deps
    - Yum repo with any non-base epel packages
    - kickstart file with package list
    - LiveCD-Creator or Pungi; Also Revisor seems to help make this easier
    - LiveISO vs. Install ISO (might cover both)

- Packages (These just go in the kickstart, shouldn't nest these packages here)
    - snort
    - bro
    - elasticsearch
    - logstash
    - nginx
    - kibana
    - netsniff-ng
    - something for netflow:
        - nfdump -> bro?
        - nfsen?
        - bro netflow analyzer
        - http://www.jtbr.cc/elastic-search-kibana3-netflow/
    - iptables
    - ssh
    - ipsec (openswan?)
    - chef
    - rock-scripts (rock specific packages could be in subdirs)
        - configuration scripts
    - CLI setup script -> set answers file for chef runs
        - Enable/Disable collection tools
        - Harden ROCK instance
    - maintenance scripts
        - Adjust filesystem cleanup and data retention intervals
        - Activate/Deactivate daemons
    - rock-kibana
        - kibana customization scripts
    - rock-elasticsearch
        - rock mapping and index configuration files
    - rock-bro
        - rock focused bro scripts suitable for incident response

## Supported Platforms

This cookbook is designed to support deployment onto EL7 hosts. Testing is done
on CentOS 7 and deployment has been performed on RHEL 7 Server. Other EL7
systems should likely work without too much extra effort, but YMMV.
