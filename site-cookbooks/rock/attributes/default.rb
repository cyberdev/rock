
default['rock']['log_dir'] = '/data'

mon_ifaces = []
node['network']['interfaces'].each do |iface, vals|
  next unless vals[:encapsulation].match('Ethernet')
  next if iface == 'lo'
  next if iface == node['network']['default_interface']
  mon_ifaces.push(iface)
end

Chef::Log.debug("DEBUG: #{mon_ifaces}")
default['rock']['mon_ifaces'] = mon_ifaces

phys_cores = {}
node['cpu'].reverse_each do |procid, vals|
  Chef::Log.debug("DEBUG #{procid} (#{procid.class}): #{vals.class}")
  next if vals.is_a?(Fixnum)
  Chef::Log.debug("CAPES DEBUG #{vals['core_id']} => #{procid}")
  phys_cores[vals['core_id']] = procid
end

# Exclude the first core for use by the OS
phys_cores = phys_cores.values.sort
phys_cores = phys_cores[1..phys_cores.length]

Chef::Log.debug("DEBUG: phys_cores == #{phys_cores}")

default['rock']['phys_cores'] = phys_cores
