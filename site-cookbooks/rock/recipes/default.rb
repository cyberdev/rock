#
# Cookbook Name:: rock
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

directory node['rock']['log_dir'] do
  owner 'root'
  group 'root'
  mode  '0755'
  action :create
end
