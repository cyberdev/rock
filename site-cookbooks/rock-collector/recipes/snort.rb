#
# Cookbook Name:: rock-collector
# Recipe:: snort
#
# Copyright 2015, Derek Ditch
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'snort::install'
include_recipe 'snort::configure'

execute 'set capabilities on snort' do
  command '/usr/sbin/setcap cap_net_raw,cap_net_admin=eip $(readlink -f /usr/sbin/snort)'
  not_if '/usr/sbin/setcap -v -q cap_net_raw,cap_net_admin=eip $(readlink -f /usr/sbin/snort)'
end

#include_recipe 'snort::rules'

file File.join(node['snort']['confdir'],'rules/local.rules') do
  mode '0644'
  owner 'root'
  group 'root'
  action :touch
end

include_recipe 'pulledpork::default'


directory node['snort']['logdir'] do
  owner node['snort']['user']
  group 'root'
  mode '0755'
  action :create
end

include_recipe 'snort::service'
