#
# Cookbook Name:: rock-collector
# Recipe:: default
#
# Copyright 2015, Derek Ditch
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'rock::default'
include_recipe 'rock-collector::interface'
# Disable pfring for now since packages aren't configured to use it
#include_recipe 'rock-collector::pfring'
include_recipe 'rock-collector::snort'
include_recipe 'rock-collector::bro'

rock_site_dir = File.join(node['bro']['install_dir'], 'share/bro/site/rock')

directory rock_site_dir do
  group node['bro']['group']
  owner 'root'
  mode '0755'
  action :create
  recursive true
end

# Point bro to the snort logs
template File.join(rock_site_dir, 'unified2.bro') do
  source 'unified2_rock.erb'
  owner 'root'
  group node['bro']['group']
  mode '664'
end

local_bro = File.join(node['bro']['install_dir'], 'share/bro/site/local.bro')

ruby_block "local.bro unifed2_rock" do
  block do
    file = Chef::Util::FileEdit.new(local_bro)
    file.insert_line_if_no_match('/@load rock/unified2/', '@load rock/unified2')
    file.write_file
  end
end
