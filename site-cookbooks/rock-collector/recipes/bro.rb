#
# Cookbook Name:: rock-collector
# Recipe:: bro
#
# Copyright 2015, Derek Ditch
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'bro'

execute 'set capabilities on bro' do
  command '/usr/sbin/setcap cap_net_raw,cap_net_admin=eip $(readlink -f /opt/bro/bin/bro)'
  not_if '/usr/sbin/setcap -v -q cap_net_raw,cap_net_admin=eip $(readlink -f /opt/bro/bin/bro)'
end

execute 'set capabilities on capstats' do
  command '/usr/sbin/setcap cap_net_raw,cap_net_admin=eip $(readlink -f /opt/bro/bin/capstats)'
  not_if '/usr/sbin/setcap -v -q cap_net_raw,cap_net_admin=eip $(readlink -f /opt/bro/bin/capstats)'
end

# Fix dir ownership
directory File.join(node['bro']['install_dir'], 'share/broctl/scripts') do
  owner node['bro']['user']
  group 'root'
  mode '0755'
  action :create
end

directory node['bro']['log_dir'] do
  owner node['bro']['user']
  group 'root'
  mode '0755'
  action :create
  recursive true
end

directory node['bro']['spool_dir'] do
  owner node['bro']['user']
  group 'root'
  mode '0755'
  action :create
  recursive true
end
