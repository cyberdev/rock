#
# Cookbook Name:: rock
# Recipe:: interface
#
# Copyright (C) 2015 Derek Ditch
#
# See LICENSE in top project dir


# For each monitor-interface; do
#  - Check driver, update intel drivers
#  - Configure interface using system net config
#  - Use ethtool for tuning interface driver


case node['platform_family']
when 'debian'
	# Meh
when 'rhel'

  service 'network' do
    action :nothing
    supports :status => true, :start => true, :stop => true, :restart => true
  end

  execute 'sysctl_reload' do
    command '/sbin/sysctl --system'
    action :nothing
  end

  template '/etc/sysctl.d/10-disable-ipv6-mon_dev.conf' do
    source 'sysctl.d-interfaces.erb'
    owner 'root'
    group 'root'
    mode '0644'
    notifies :run, 'execute[sysctl_reload]', :immediately
  end

	# Create /sbin/ifup-local with monitor interfaces
	template '/sbin/ifup-local' do
		source 'ifup-local.erb'
		owner 'root'
		group 'root'
		mode '0755'
    notifies :restart, 'service[network]', :delayed
	end

  node['rock']['mon_ifaces'].each do |interface|
    template "/etc/sysconfig/network-scripts/ifcfg-#{interface}" do
      source 'sysconfig-ifcfg.erb'
      owner 'root'
      group 'root'
      mode '0644'
      variables({ 'interface' => interface })
      notifies :restart, 'service[network]', :delayed
    end
  end

end
