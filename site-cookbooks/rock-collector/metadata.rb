name             'rock-collector'
maintainer       'Derek Ditch'
maintainer_email 'derek.ditch@gmail.com'
license          'Apache 2.0'
description      'Installs/Configures rock-collector'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'rock'
depends 'pfring'
depends 'yum-epel'
depends 'snort'
depends 'pulledpork'
depends 'bro'
