
# These values go into /etc/sysconfig/snort
normal['snort']['interface']   = node['rock']['mon_ifaces']
normal['snort']['logdir']      = File.join(node['rock']['log_dir'], 'snort')
normal['snort']['oinkcode']    = '32ee6a841cfc6951b392b8ad930b9ef758724fc3'

# Use only unifed2 output
normal['snort']['alertmode']   = ''
normal['snort']['binary_log']  = false

# This is a hack for the current init script to move the pid file and fix umask
normal['snort']['bpf']         = '-m 0133 --pid-path /var/log/snort ip or not ip'
normal['pulledpork']['rule_url_array'] = [
  'https://s3.amazonaws.com/snort-org/www/rules/community/|community-rules.tar.gz|Community',
  'https://www.snort.org/reg-rules/|snortrules-snapshot-2975.tar.gz|796f26a2188c4c953ced38ff3ec899d8ae543350',
  'https://www.snort.org/reg-rules/|opensource.gz|796f26a2188c4c953ced38ff3ec899d8ae543350'
]
