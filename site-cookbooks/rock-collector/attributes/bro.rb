include_attribute 'rock::default'

normal['bro']['platform']['type'] = 'centos'
normal['bro']['platform']['version'] = '7'

normal['bro']['install_type'] = 'cluster'

normal['bro']['cluster'] = [
  {
    'name' => node['hostname'] + '-manager',
    'type' => 'manager',
    'host'=> '127.0.0.1'
  },
  {
    'name' => node['hostname'] + '-proxy',
    'type'=> 'proxy',
    'host'=> '127.0.0.1'
  }
]

node['rock']['mon_ifaces'].each do |iface|
  section =  {
        'name'=> node['hostname'],
        'type'=> 'worker',
        'interface'=> iface,
        'host'=> '127.0.0.1'
  }

  # if node['rock']['phys_cores'].length > 1 then
  #   section.merge!( {
  #     'lb_method'=> 'pf_ring',
  #     'lb_procs'=> node['rock']['phys_cores'].length,
  #     'pin_cpus'=> node['rock']['phys_cores'].join(',')
  #   })
  # end

  normal['bro']['cluster'].push ( section )
end


normal['bro']['log_dir'] = File.join(node['rock']['log_dir'], 'bro/logs')
normal['bro']['spool_dir'] = File.join(node['rock']['log_dir'], 'bro/spool')
