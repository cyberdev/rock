rock-collector
  - pfring
  - bro
  - snort
  - stenographer

rock-database
  - kafka
  - logstash
  - elasticsearch

rock-analysis
  - elasticsearch
  - kibana
  - irb/ipython thing

rock-allinone
  - Bro
  - snort
  - stenographer
  - kafka
  - logstash
  - elasticsearch
  - kibana
